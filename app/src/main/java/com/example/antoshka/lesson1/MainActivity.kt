package com.example.antoshka.lesson1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d("lesson2", "onCreate")

        if ( savedInstanceState == null ) {
            Log.d("lesson2", "приложение запущено впервые")
        } else {
            Log.d("lesson2", "приложение восстановлено из памяти")
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("lesson2", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d("lesson2", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d("lesson2", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d("lesson2", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("lesson2", "onDestroy")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("lesson2", "onRestart")
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        Log.d("lesson2", "onSaveInstanceState")
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        Log.d("lesson2", "onRestoreInstanceState")
    }
}
